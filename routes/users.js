var express = require('express');
var router = express.Router();
var db = require('libs/db').db;

router.get('/', function(req, res, next) {
  printUsers(function (text) {
    res.send(text);
    //res.end();
  })
});

function printUsers(callBack){
  db.query('select * from user', function (err, rows, field) {
    if(err){
      console.log("DB error users.js.PrintUsers")
      callBack("Error");
    }else{
      var answer = "";
      for(var i = 0; i < rows.length; i++){
        answer += rows[i]['SecondName'] + ' ';
        answer += rows[i]['FirstName'] + ' ';
        answer += rows[i]['MiddleName'] + ' ';
        answer += rows[i]['Id'] + ' ';
        answer += '<br>';
      }
      callBack(answer);
    }
  })
}
module.exports = router;
