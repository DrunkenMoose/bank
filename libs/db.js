var mysql = require('mysql');
var config = require('config');

var db = mysql.createConnection({
    host: config.get("host"),
    user: config.get("user"),
    password: config.get("password"),
    database: config.get("database"),
});

exports.db = db;